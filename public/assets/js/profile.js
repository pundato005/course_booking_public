//get the value of the access token inside the local storage and place it inside a new variable. 
let token = localStorage.getItem("token"); 
// console.log(token)

let lalagyan = document.querySelector("#profileContainer")
//our goal here is to display the information about the user details 
//send a request to the backend project
fetch('http://localhost:4000/api/users/details', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`
  }
}).then(res => res.json())
.then(jsonData => {
  //lets create a checker to make sure the fetch is successful
  //console.log(jsonData)

  //get all the elements inside the enrollments array.
  console.log(jsonData.enrollments)
  let userSubjects = jsonData.enrollments.map(subject => {
    console.log(subject)
    return (
       //i want to create a container that will serve as the html boilerplate that will display the properties of the subject that want to get. 
        `
          <tr>
            <td> ${subject.courseId}</td>
            <td> ${subject.enrolledOn}</td>
            <td> ${subject.status}</td>
          </tr>
        `
      )
  }).join("") //this will remove the commas


  //how are we going to display the information inside the front end component? 
  //lets target the div element first using its id attribute
  //lets create a section to display all the courses the user is enrolled in
    lalagyan.innerHTML = `
       <div class="col-md-13">
          <section class="jumbotron my-5">
             <h3 class="text-center">First Name: ${jsonData.firstName}</h3>
             <h3 class="text-center">Last Name: ${jsonData.lastName}</h3>
             <h3 class="text-center">Email:     ${jsonData.email}</h3>
             <h3 class="text-center">Mobile Number:  ${jsonData.mobileNo}</h3>
               <table class="table">
                <tr>
                  <th>Course ID:</th>
                  <th>Enrolled On:</th>
                  <th>Status: </th>
                  <tbody> ${userSubjects} </tbody>
                </tr>
               </table>

          </section>
       </div>

    `
})
